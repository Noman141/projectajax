<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');


Route::resource('/getcontact', 'ContactController');

Route::get('/contact','ContactController@index')->name('contact');

Route::post('/contact/store','ContactController@store')->name('contact.store');

Route::get('/contact/edit/{id}','ContactController@edit')->name('contact.edit');

Route::get('/contact/update/{id}','ContactController@update')->name('contact.update');

Route::get('/contact/all','ContactController@allContact')->name('contact.all');