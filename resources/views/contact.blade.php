<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Contact Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
</head>
<body>
<div class="container mt-5">
    <h2>CRUD by Ajax and Laravel</h2>
    <div class="mt-3">
        <a onclick="addForm()" class="btn btn-sm btn-success my-3">Add New</a>
        <table id="contactTable" class="table table-bordered table-dark">
            <thead>
                <tr>
                    <th scope="col">Serial</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Religion</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody class="text-dark">
            </tbody>
        </table>
    </div>
    @include('form')
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.10.2/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>
    var table =$('#contactTable').DataTable({
               processing:true,
               serverSide:true,
               ajax: " {{ route('contact.all') }}",
               columns: [
                   { data:'id',name:'id'},
                   { data:'name',name:'name'},
                   { data:'email',name:'email'},
                   { data:'phone',name:'phone'},
                   { data:'religion',name:'religion'},
                   { data:'action',name:'action',orderable:false,searchable:false}
               ]
    });
//    add form function will go here
    function addForm() {
        save_method = 'add';
        $('input[name_method]').val('POST');
        $('#modalForm').modal('show');
        $('#modalForm form')[0].reset();
        $('#modalTitle').text('Add Contact');
        $('#insertContactButton').text('Add Contact');
    }
//    insert data by ajax with laravel

    $(function () {
        $('#modalForm form').validator().on('submit',function (e) {
            if (!e.isDefaultPrevented()){
                var id = $("#id").val();
                if (save_method == 'add')url = "{{ route('contact.store') }}";
                else url = "{{ url('/getcontact')}}"+ '/'+ id;
                $.ajax({
                    url  : url,
                    type : "POST",
                    data : new FormData($("#modalForm form")[0]),
                    contentType: false,
                    processData: false,
                    success : function (data) {
                        $('#modalForm').modal('hide');
                        table.ajax.reload();
                        swal({
                            title:"Good job!",
                            text: "You are clicked the button!",
                            icon: "success",
                            button: "Grate!"
                        })
                    },
                    error : function (data) {
                        swal({
                            title:"Oops...!",
                            text: data.message,
                            icon: "error",
                            timer :1500
                        })
                    }
                });
                return false;
            }
        })
    });
//    edit data by Ajax with laravel
    function editData(id) {
        save_method='edit';
        $('input[name_method]').val('PATCH');
        $('#modalForm form')[0].reset();
        $.ajax({
            url: "{{ url('/getcontact') }}"+'/'+ id + "/edit",
            type : "GET",
            dataType : "JSON",
            contentType: "application/json;charset=utf-8",
              success : function (data) {
                $("#modalForm").modal("show");
                $("#modalTitle").text("Edit Contact");
                $("#insertContactButton").text("Update Contact");
                $("#id").val(data.id);
                $("#name").val(data.name);
                $("#email").val(data.email);
                $("#phone").val(data.phone);
                $("#religion").val(data.religion);
              },
              error : function () {
                  alert("Something went wrong");
              }
        });
    }
//Delete method
    function deleteData(id) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then(willDelete => {
               if (willDelete) {
                $.ajax({
                   url: "{{ url('/getcontact') }}"+ '/'+id,
                   type: "POST",
                   data : {'_method':'DELETE', _token: '{{csrf_token()}}'},
                   success: function(data){
                    table.ajax.reload();
                    swal({
                        title: "Delete Done!",
                        text: "You clicked the button!",
                        icon : "success",
                        button:"Done"
                    })

                   },
                   error: function(data){
                    swal({
                        title: "Oops...!",
                        text: data.message,
                        icon: 'error',
                        timer: 1500
                    })
                   }

                })
               }else{
                swal("Your file is safe!")
               }
        })
    }

    // Show Method

    function showData(id) {
        $.ajax({
            url: "{{ url('/getcontact') }}"+ '/'+id,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#modalFormForSingleData').modal('show'),
                $('#modalForSingleData').text(data.name+'\'s' +' '+ 'Informations'),
                $('#contactID').text(data.id),
                $('#contactName').text(data.name),
                $('#contactEmail').text(data.email),
                $('#contactPhone').text(data.phone),
                $('#contactReligion').text(data.religion)
            },
            error: function(){
                alert("Something Wrong!")
            }
        })
    }

</script>
</body>
</html>