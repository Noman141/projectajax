var table =$('#contactTable').DataTable({
processing:true,
serverSide:true,
ajax: " {{ route('contact.all') }}",
columns: [
{ data:'id',name:'id'},
{ data:'name',name:'name'},
{ data:'email',name:'email'},
{ data:'phone',name:'phone'},
{ data:'religion',name:'religion'},
{ data:'action',name:'action',orderable:false,searchable:false}
]
});
//    add form function will go here
function addForm() {
save_method = 'add';
$('input[name_method]').val('POST');
$('#modalForm').modal('show');
$('#modalForm form')[0].reset();
$('#modalTitle').text('Add Contact');
$('#insertContactButton').text('Add Contact');
}
//    insert data by ajax with laravel

$(function () {
$('#modalForm form').validator().on('submit',function (e) {
if (!e.isDefaultPrevented()){
var id = $("#id").val();
if (save_method == 'add')url = "{{ url('/contact/store') }}";
else url = "{{ url('/contact/store') .'/'}}"+id;
$.ajax({
url: url,
type: "post",
data: new FormData($("#modalForm form")[0]),
contentType: false,
processData: false,
success : function (data) {
$('#modalForm').modal('hide');
table.ajax.reload();
swal({
title:"Good job!",
text: "You are clicked the button!",
icon: "success",
button: "Grate!"
})
},
error : function (data) {
swal({
title:"Oops...!",
text: data.message,
icon: "error",
timer :1500
})
}
});
return false;
}
})
});
//    edit data function will go here
function editData(id) {
save_method = 'edit';
$('input[name_method]').val('PATCH');
$('#modalForm form ')[0].reset();
$.ajax({
url: "{{ url('/contact') }}" + '/' + id + "/edit",
type: "GET",
dataType: "jSON",
success: function (data) {
$('#modalForm').modal('show');
$('#modalTitle').text('Edit Contact');
$('#insertContactButton').text('Update Contact');
$('#id').val(data.id);
$('#name').val(data.name);
$('#email').val(data.email);
$('#phone').val(data.phone);
$('#religion').val(data.religion);
},
error: function () {
alert('Somethis wents wrong!')
}
})
}